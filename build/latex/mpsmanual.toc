\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}System Description}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Front Panel Elements}{4}{section.1.1}%
\contentsline {section}{\numberline {1.2}Back Panel Elements}{5}{section.1.2}%
\contentsline {chapter}{\numberline {2}Unpacking the System}{7}{chapter.2}%
\contentsline {chapter}{\numberline {3}Setting Up the System}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Setting up the MPS for Operation}{9}{section.3.1}%
\contentsline {section}{\numberline {3.2}Turning ON the MPS}{11}{section.3.2}%
\contentsline {chapter}{\numberline {4}Operation}{15}{chapter.4}%
\contentsline {section}{\numberline {4.1}Setting the Output Frequency}{16}{section.4.1}%
\contentsline {section}{\numberline {4.2}Setting the Output Power}{16}{section.4.2}%
\contentsline {section}{\numberline {4.3}Switching between EPR and DNP Operation}{16}{section.4.3}%
\contentsline {section}{\numberline {4.4}Enabling/Disabling the Output Power}{18}{section.4.4}%
\contentsline {section}{\numberline {4.5}Using the External Trigger (optional)}{18}{section.4.5}%
\contentsline {section}{\numberline {4.6}Entering the Advanced View}{19}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}Power Monitoring (Rx and Tx)}{19}{subsection.4.6.1}%
\contentsline {subsection}{\numberline {4.6.2}Tuning the EPR Resonator}{20}{subsection.4.6.2}%
\contentsline {subsection}{\numberline {4.6.3}Enabling the Lock Feature}{21}{subsection.4.6.3}%
\contentsline {subsection}{\numberline {4.6.4}Microwave Amplifier Temperature Monitor}{22}{subsection.4.6.4}%
\contentsline {chapter}{\numberline {5}Remote Interface}{23}{chapter.5}%
\contentsline {section}{\numberline {5.1}Serial Port Configuration}{24}{section.5.1}%
\contentsline {section}{\numberline {5.2}Communicating with the MPS}{24}{section.5.2}%
\contentsline {section}{\numberline {5.3}Command Syntax}{25}{section.5.3}%
\contentsline {section}{\numberline {5.4}Python Package to Control the MPS}{25}{section.5.4}%
\contentsline {section}{\numberline {5.5}List of Available Commands}{25}{section.5.5}%
\contentsline {subsection}{\numberline {5.5.1}Example with pySerial}{27}{subsection.5.5.1}%
\contentsline {chapter}{\numberline {6}Maintenance}{29}{chapter.6}%
\contentsline {section}{\numberline {6.1}Firmware Upgrade}{29}{section.6.1}%
\contentsline {chapter}{\numberline {7}System Specifications}{31}{chapter.7}%
\contentsline {chapter}{\numberline {8}Appendix}{33}{chapter.8}%
\contentsline {section}{\numberline {8.1}Conversion Table: dBm to W}{33}{section.8.1}%
\contentsline {chapter}{\numberline {9}The Fineprint}{35}{chapter.9}%
\contentsline {section}{\numberline {9.1}Notices}{35}{section.9.1}%
\contentsline {section}{\numberline {9.2}Warranty}{36}{section.9.2}%
\contentsline {subsection}{\numberline {9.2.1}Technology License}{36}{subsection.9.2.1}%
\contentsline {chapter}{\numberline {10}Index}{37}{chapter.10}%
\contentsline {chapter}{Index}{39}{section*.4}%
