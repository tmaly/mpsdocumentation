======================
The :index:`Fineprint`
======================

No part of this manual may be reproduced in any form or by any means (including electronic storage and retrieval or translation into a foreign language) without prior agreement and written consent from Bridge12 Technologies, Inc. as governed by United States and international copyright laws. 

Bridge12 Technologies, Inc., 37 Loring Drive, Framingham, MA 01702, USA
http://www.bridge12.com

----------------
:index:`Notices`
----------------

.. note::
    A :index:`NOTE` notice denotes a hazard. It calls attention to an operating procedure, practice, or the like that, if not correctly performed or adhered to, could result in damage to the product or loss of important data. Do not proceed beyond an IMPORTANT notice until the indicated conditions are fully understood and met.

.. warning::
    A :index:`WARNING` notice denotes a hazard. Calling attention to an operating procedure, practice, or the like that, if not correctly performed or adhered to, could result in personal injury or death. Do not proceed beyond a WARNING notice until the indicated conditions are fully understood and met.

--------
Warranty
--------

The material contained in this document is provided “as is” and is subject to changes without further notice. Further, to the maximum extent permitted by applicable law, Bridge12 disclaims all warranties, either express or implied, regarding this manual and any information contained herein, including but not limited to, the implied warranties of merchantability and fitness for a particular purpose.

Bridge12 shall not be liable for errors or for incidental or consequential damages in connection with the furnishing, use, or performance of this document, or of any information contained herein.

Technology License
==================

The hardware and/or software described in this document are furnished under a license and may be used or copied only in accordance with the terms of such license.