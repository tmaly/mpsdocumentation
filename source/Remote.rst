=========================
:index:`Remote Interface`
=========================

.. |pyB12MPS| raw:: html

   <a href="http://http://pyb12mps.bridge12.com/" target="_blank"> pyB12MPS</a>


The Bridge12 MPS is equipped with a :index:`Serial Interface` to allow control of the system remotely from a computer. Communication is enabled using standard serial communication (RS232) over USB.

.. warning::

    The remote interface should only be used by users familiar with the operation of the MPS. Please contact Bridge12 at support@bridge12.com if you have any questions about the serial interface.

-------------------------
Serial Port Configuration
-------------------------

The Serial Interface is configured with the following operation parameters:

.. _RI_SerialPortConfiguration:
.. list-table:: Serial Port Configuration
   :widths: 2 1
   :header-rows: 1
   :align: center

   * - Parameter
     - Value
   * - Baud Rate
     - 115200
   * - Data Bits
     - 8
   * - Parity Bit
     - No
   * - Stop Bit
     - 1

It is possible to communicate with the MPS through any serial terminal. Make sure the baud rate, number of data bits, number of parity bits, and the number of stop bits is set to the correct values (see :numref:`RI_SerialPortConfiguration`).

--------------------------
Communicating with the MPS
--------------------------

Most likely, the Bridge12 MPS will perform a reset at the time the serial connection (socket) is established. This is very normal behavior of the interface. When developing customer applications to control the MPS through the serial interface, the serial socket should only be opened once when the customer program goes through its initialization routine. The serial socket is kept open until the customer application closes. Closing the serial socket is important, otherwise the remote computer may report an error.

When opening the serial socket the system will respond with the following message:

.. code-block:: console

    Bridge12 MPS Started

Once the system is initialized and ready the system will respond with the following message:

.. code-block:: console

    System Ready

The user needs to wait for the System Ready statement before issuing any command. All commands that are issued before the System Ready message will be ignored.

---------------
Command Syntax
---------------

Commands are not case sensitive. When opening a serial socket to the MPS, the microcontroller will reset. Therefore, the serial socket should only be opened once and closed only after the last command is sent to the unit.

The general structure for sending a command is:

.. code-block:: console

    COMMAND VALUE <CR>

The general structure to query a value is:

.. code-block:: console

    COMMAND? <CR>

---------------------------------
Python Package to Control the MPS
---------------------------------

Bridge12 has developed a python package to control the MPS. The python package can be installed as a python module and its functionality can be intregrated into user specific applications.

For more information about the package please visit |pyB12MPS|.

--------------------------
List of Available Commands
--------------------------

Currently the following commands are implemented. If a special command is required, please contact Bridge12 at support@bridge12.com.

.. _RI_SerialCommands:
.. list-table:: Serial Port Configuration
   :widths: 1 2
   :header-rows: 1
   :align: center

   * - Command
     - Description
   * - help
     - Get help instructions on serial port
   * - :index:`freq`
     - Set/query microwave frequency in kHz
   * - :index:`power`
     - Set/query microwave power in dBm times 10
   * - :index:`rxpowermv`
     - Query Rx diode reading in mV times 10
   * - :index:`rxpowerdbm`
     - Query Rx diode power in dBm times 10
   * - :index:`txpowermv`
     - Query Tx diode reading in mV times 10
   * - :index:`txpowerdbm`
     - Query Tx diode power in dBm times 10
   * - :index:`rfstatus`
     - Set/Query RF status
   * - :index:`wgstatus`
     - Set/Query Waveguide status
   * - :index:`ampstatus`
     - Set/Query amplifier power
   * - :index:`amptemp`
     - Query amplifier temperature
   * - :index:`lockstatus`
     - Set/Query frequency lock
   * - :index:`lockdelay`
     - Set/Query frequency lock delay in ms
   * - :index:`lockstep`
     - Set/Query frequency lock step in kHz
   * - :index:`screen`
     - Set/Query frequency lock step in kHz
   * - :index:`firmware`
     - Query firmware version
   * - :index:`id`
     - Query MPS ID number
   * - :index:`serial`
     - Query serial number
   * - :index:`rxdiodessn`
     - Query Rx diode serial number
   * - :index:`txdiodessn`
     - Query Tx diode serial number
   * - :index:`debug`
     -  Set debug mode on/off


Example with pySerial
---------------------

Below is an example about how to use the MPS serial interface using pySerial. A MPS specific python package (|pyb12mps|) is available for more convenient remote control of the MPS.

::
    
    import serial
    import time

    # MPS Serial Port
    serialPort = 'COM5'

    # Start Serial Connection
    ser = serial.Serial(serialPort,115200,timeout = 1.)
    time.sleep(0.1)

    system_ready_string = 'Synthesizer detected'

    # Initialize MPS
    while not is_system_ready:
        time.sleep(0.1)
        bytes_in_waiting = ser.in_waiting
        if bytes_in_waiting > 0:
            read_bytes = ser.readline()
            read_string = read_bytes.decode('utf-8').rstrip()
            print(read_string)
            if read_string == system_ready_string:
                is_system_ready = True

    # Command to send to MPS
    command = 'freq 9400000\n'

    # Encode MPS command (convert to bytes)
    command_bytes = command.encode('utf-8')

    # Send command to MPS
    ser.write(command_bytes)

