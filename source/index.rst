.. MPS Documentation documentation master file, created by
   sphinx-quickstart on Fri Apr 24 14:30:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================================
Bridge12 MPS for ODNP Spectroscopy
==================================

.. |bridge12Link| raw:: html

   <a href="http://www.bridge12.com/" target="_blank"> Bridge12</a>


.. |bridge12MPSLink| raw:: html

   <a href="http://www.bridge12.com/bridge12-mps-microwave-source-for-9-ghz-odnp-experiments/" target="_blank"> Bridge12 MPS</a>


.. |spinCoreRadioProcessorBoard| raw:: html

   <a href="http://www.spincore.com/products/RadioProcessor/" target="_blank"> Radio Processor Board</a>


.. _Index_FrontPanel:
.. figure:: _static/images/FrontPanel_20200514.png
    :width: 600
    :alt: MPS Front Panel
    :align: center

    The Bridge12 Microwave Power Source (MPS) for ODNP Spectrosopy


The Bridge12 MPS (:numref:`Index_FrontPanel`) is a Microwave Power Source (MPS) for Overhauser Dynamic Nuclear Polarization (ODNP) Spectroscopy.

Further detailed technical information about the MPS can be found by visiting the |bridge12MPSLink| webiste.



.. toctree::
   :maxdepth: 2
   :numbered:                       
   :caption: Contents

   Description
   Unpacking
   Setup
   Operation
   Remote
   Maintenance
   Specifications
   Appendix
   Fineprint


Index
=====

* :ref:`genindex`
