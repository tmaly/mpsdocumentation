===========================
:index:`System Description`
===========================

.. image:: _static/images/Banner_20200513.png
    :width: 600
    :alt: MPS Front Panel
    :align: center


The Bridge12 Microwave Power Source (MPS) is a compact, high-power microwave source for X-Band (9 GHz) Overhauser Dynamic Nuclear Polarization (O-DNP) spectroscopy.
It is a turn-key, easy to operate, stand-alone microwave source. The microwave radiation is generated using a phase-locked digital synthesizer and amplified using a high-power microwave amplifier. The system constantly monitors the forward and reflected power and can tolerate 100% reflected power without being damaged.
The system can control an external waveguide switch to alternate between EPR and DNP operation. The Bridge12 MPS is a versatile research instrument which can be operated as a stand-alone unit, or integrated into an existing EPR spectrometer.


--------------------
Front Panel Elements
--------------------

.. _SD_FrontPanel:
.. figure:: _static/images/MPSFrontPanel_20200513.png
    :width: 600
    :alt: MPS Front Panel
    :align: center

    MPS Front Panel

The following control and display elements of the Bridge12 MPS are located on the front panel (see :numref:`SD_FrontPanel`)

1.  5” TFT Display
2.  Buttons to control the frequency or microwave power
#.  Rotary encode
#.  Switch to select between EPR and DNP operation
#.  Microwave ON/OFF button
#.  Waveguide switch and DIO connector
#.  SMA breakout cable
#.  Front panel connectors for Receiver Monitor (Rx, analog), Transmitter Monitor (Tx, analog), Sweep Trigger (digital, TTL), External Trigger (digital, TTL), External 10 MHz reference input
#.  RF Output (SMA connection)

-------------------
Back Panel Elements
-------------------

.. _SD_BackPanel:
.. figure:: _static/images/MPSBackPanel_20200424.png
    :width: 600
    :alt: MPS Front Panel
    :align: center

    MPS Back Panel

The following elements are located on the back pane (see :numref:`SD_BackPanel`)

1.	System power ON/OFF switch
2.	Fuse
3.	Power connection
4.	USB connection (A type)

.. warning::
    Only use the power supply delivered with the system for powering the MPS.

    The USB connection is for remote operation only. Do not connect any other auxiliary devices (e.g. phones, tablets etc.) to provide power.
