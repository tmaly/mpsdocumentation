==============================
:index:`System Specifications`
==============================

System Specifications given here are general specifications. Individual system specifications may differ for each individual system.


.. _SS_SystemSpecifications:
.. list-table:: System Specifications (Default Configuration)
   :widths: 1 2
   :header-rows: 1
   :align: center

   * - Property
     - Value
   * - :index:`Frequency Range`
     - 9.4 - 10.5 GHz
   * - Maximum Output Power
     - `+` 40 dBm (10 W)
   * - Maximum Reflected Power
     - `+` 33 dBm (2 W)
   * - :index:`Power Resolution`
     - `+/-` 0.1 dBm
   * - Shortest Pulse Length
     - 100 ns
   * - :index:`System Weight`
     - 3.3 lbs (1.5 kg)
   * - :index:`Physical Dimensions`
     - 12 x 7 x 14 inch (w, l, h)
   * - 
     - 30.5 x 17.8 x 35.6 cm (w, l, h)
   * - :index:`Input Power`
     - 90 - 264 VAC

In its standard configuration the systems comes with a TFT front panel dispaly, a SMA breakout connection to connect a fast microwave switch or an Arbitrary Waveform Generator (AWG), a connector to connect the waveguide switch, and a front panel for custom digital input/output signals. In addition, the system accepts an external trigger, to modulate the microwave power, an external 10 MHz reference and a sweep trigger.

The system is also avaialbe with custom modifications. For more information contact Bridge12 at info@bridge12.com.