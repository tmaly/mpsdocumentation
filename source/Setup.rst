==============================
:index:`Setting Up the System`
==============================

.. Warning::
    Do not enable the microwave power without a load (e.g. tuned EPR cavity) attached to the output. When no load is connected, the output power is reflected into the system and can lead to permanent damages.


--------------------------------
Setting up the MPS for Operation
--------------------------------

To setup the system for operation, follow these steps:

1.	Unpack the power supply and connect it to the microwave source. The power supply is connected to the back of the system (see :numref:`Setup_PowerConnector`).

.. _Setup_PowerConnector:
.. figure:: _static/images/PowerConnection_20200513.png
    :width: 600
    :alt: MPS Front Panel
    :align: center
    
    Back panel power connector

Make the following connections as shown in :numref:`Setup_FrontConnection`:

2.	Connect the SMA to the SMA front panel connector labeled “MW OUT” (Panel A). The other end of the SMA cable is connected to the SMA to WR-90 adapter of the connection kit, connecting the waveguide switch to the Bridge12 MPS (see the Connection Kit manual for details).
3.	Install the 3” semi-rigid jumper cable to the front of the system (Panel B)
4.	Connect the cable controlling the waveguide switch to the connector labeled WG switch (Panel C).

.. _Setup_FrontConnection:
.. figure:: _static/images/FrontPanelConnections_20200513.png
    :width: 400
    :alt: MPS Front Panel
    :align: center

    Microwave connections for SMA breakout and resonator. Connection to the automated waveguide switch

------------------
Turning ON the MPS
------------------

Once all connections are made, turn on the system by pressing the power button located on the back panel of the MPS. The green light will come on, indicating that the system is switched on (note, that the actual color of the light can be different. Some system will have a blue indicator.)

When the MPS is powered on, the system will go through an initialization process. During this process the front screen will display the Bridge12 logo (see Figure 8) and other information, such as the serial number and the version number of the currently installed firmware.

.. figure:: _static/images/StartupScreen.png
    :width: 600
    :alt: MPS Front Panel
    :align: center

    Bridge12 MPS startup screen

Once the initialization process is completed, the front panel display of the Bridge12 MPS will switch to its default display mode.

.. figure:: _static/images/MainScreen_20200514.png
    :width: 600
    :alt: MPS Front Panel
    :align: center

    Front panel display of main screen. The system is ready.

For safety reasons, the output power is always disabled after the MPS is switched on. Once the main screeen is displayed the system is ready.