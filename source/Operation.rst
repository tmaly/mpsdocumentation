==================
:index:`Operation`
==================

The Bridge12 MPS can be completely operated from the front panel. To the right of the TFT display are three silver push buttons and a dial (rotary encoder) that can be rotated in either direction. The top two buttons are used to either select and  change the frequency, or select and change the power level (see :numref:`Operation_FrequencyPower`).

.. _Operation_FrequencyPower:
.. figure:: _static/images/FrequencyPower_20200514.png
    :width: 600
    :alt: MPS Front Panel
    :align: center
    
    Left: MPS set to change the output frequency. Right: MPS set to change the output power level

-------------------------------------
Setting the :index:`Output Frequency`
-------------------------------------

To set the microwave output frequency:

1.	Push the top button located to the right of the screen. The background color of the frequency will change from green to white (see  :numref:`Operation_FrequencyPower`, left).
2.	Using the rotary encoder, dial in the desired output frequency. Turn the dial clockwise to increase the frequency and counter clockwise to decrease the frequency. The step size to increase or decrease the frequency will change depending on how fast the dial is turned. For large steps, quickly turn the dial. For fine adjustments, turn the dial one click at a time.

---------------------------------
Setting the :index:`Output Power`
---------------------------------

To set the microwave output power:

1.	Push the middle button located to the right of the screen. The background color of the amplitude will change from green to white (see  :numref:`Operation_FrequencyPower`, right).
2.	Using the rotary encoder, dial in the desired output power. Turn the dial clockwise to increase the output power level and counter clockwise to decrease the output power level. The step size to increase or decrease the power level will change depending on how fast the dial is turned. For large steps, quickly turn the dial. For fine adjustments, turn the dial one click at a time.

---------------------------------------------------------
Switching between :index:`EPR` and :index:`DNP` Operation
---------------------------------------------------------

.. warning::
    Do not enable the microwave power without a load (e.g. tuned EPR cavity) attached to the output. When no load is connected, the output power is reflected into the system and can lead to permanent damages.

The Bridge12 MPS can control an external waveguide switch. The waveguide switch is part of the optional Bridge12 ODNP Connection Kit and is installed between the EPR bridge and the EPR resonator to easily switch between EPR and DNP operation. For details about the Connection Kit and how to install it, please consult the Connection Kit manual.

.. _Operation_EPRDNPOperation:
.. figure:: _static/images/EPRDNPOperation_20200514.png
    :width: 600
    :alt: MPS Front Panel
    :align: center

    Left: Waveguide switch control button (orange). Right: Microwave (RF) ON/OFF button (green)

To switch to DNP operation, simply press the button labeled EPR/DNP. If your system is equipped with a waveguide switch, you will hear the waveguide switch changing direction. In any case, the orange light of the button will light up to indicate that the system is switched into DNP operation mode (see :numref:`Operation_EPRDNPOperation`). A schematic explaining the function of the waveguide switch is shown below.

.. figure:: _static/images/Figure12_171009.png
    :width: 400
    :alt: MPS Front Panel
    :align: center

    Waveguide switch postions

The waveguide switch has four ports. The way the ports are connected depends upon the position of the waveguide switch (see Figure 12, middle and right). The position of the switch is indicated on the top of the silver cap (see Figure 12 left).

.. note::
    For safety reason, the microwave power will always be disabled when the waveguide switch is switched from DNP operation to EPR. Switching back to the DNP mode will not automatically enable the microwave power.

    The microwve power can only be enabled if the waveguide switch is in the DNP position (orange button light is on). Even if your system is not equipped with a waveguide switch, the EPR/DNP button must be pressed before the microwave power can be enabled. This feature can be disabled upon special request in the firmware.

-----------------------------------
Enabling/Disabling the Output Power
-----------------------------------

By default, the output power is disabled when the Bridge12 MPS is initially powered up. To enable the microwave output power, make sure the system is in DNP mode (orange light is on) and press the button labeled RF ON/OFF. The button will light up with a green light and the status on the display will change from OFF to ON (see :numref:`OPeration_EPRDNPOperation`).
Pressing the same button again will disable the microwave output power.

-------------------------------------
Using the External Trigger (optional)
-------------------------------------

The output power of the Bridge12 MPS can be controlled (modulated) using an external trigger connected to the front panel BNC connector labeled “EXT TRIG”. This is an optional feature. Please make sure this feature is requested at the time of ordering the MPS. Please contact Bridge12 at info@bridge12.com with any questions regarding this feature.

The external trigger accepts a TTL signal (low: < 0.8 V, high: > 2 V). Please make sure to not exceed these limits. To enable the external trigger:

1.	Press and hold the microwave ON/OFF button for at least 3 seconds.
2.	The output power indicator on the display will change to EXT (with an orange background color).

The unit is now ready to be controlled by an external trigger. To return to manual mode, press the microwave ON/OFF button again.

-----------------------------------
Entering the :index:`Advanced View`
-----------------------------------

The Bridge12 MPS has an advanced view where additional system information will be displayed. In the advanced mode, the user has access to the serial number and the version number of the firmware currently installed. In addition, the temperature of the microwave power amplifier is displayed in the advanced view.

To enter the advanced view, simply press and hold down the rotary encoder for about 2 seconds. To exit the advanced view, press the rotary encoder again.

:index:`Power Monitoring` (Rx and Tx)
=====================================

The Bridge12 MPS has a built-in power monitoring circuit (optional feature) to constantly monitor the forward and reflected microwave power. An overview of the system is shown in Figure 13.

.. figure:: _static/images/Figure13_171009.png
    :width: 400
    :alt: MPS Front Panel
    :align: center

    High-level schematics of the Bridge12 MPS

The forward and reflected power is detected using microwave detection diodes that output a voltage proportional to the measured microwave power. This voltage is converted to a microwave power by the integrated microcontroller and is displayed on the advanced screen. The microwave diodes are calibrated by Bridge12. The calibration data is available upon request. Please contact Bridge12 at support@bridge12.com for further information.

As a safety feature, the MPS will disable the microwave output power if the reflected power level exceeds 33 dBm (2 W). This level can be changed upon request.

:index:`Tuning` the EPR Resonator
=================================

The power monitor can be used to monitor the reflected power from the microwave resonator. This is helpful during an ODNP experiment when using high microwave power levels to ensure the resonator is critically coupled. To monitor the reflected power, enter the advanced screen by pressing the rotary encoder and follow the instructions below:

1.	With the microwave output power disabled, dial in the resonance frequency of the empty resonator.
2.	Switch the microwave power ON and slowly increase the microwave output power to about 10 dBm. The exact value is not critical, but should not exceed 20 dBm.
3.	Slowly vary the microwave output frequency and monitor the reflected power. At a certain frequency, a clear power minimum should be detected if the resonator is critically coupled.
4.	Set the microwave frequency to a value corresponding to the minimum reflected power. This is the resonance frequency of the EPR resonator.
5.	Check that the resonator is critically coupled. Slowly (!) turn the iris screw of the resonator and observe the reflected microwave power. Ensure that the reflected power is at a minimum.
6.	Slowly increase or decrease the microwave frequency. Adjust if necessary to minimize the reflected microwave power.
7.	Increase the microwave output power and repeat steps 5 through 7.

If the microwave resonator is critically coupled, the reflected microwave power will not increase when the forward power is increased. At high output power levels (> 30 dBm, 1 W) the reflected output power may change, but can corrected by moving the microwave frequency. This is due to heating effects inside the EPR resonator, which will lead to thermal expansion of the cavity and the resonance frequency will decrease slightly.

.. note::
    During experiments, make sure a constanct flow of air through the resonator. This helps compensating for microwave induced sample heating effects.

Enabling the :index:`Lock` Feature
==================================

The Bridge12 MPS can operate in a “lock” mode which constantly minimizes the reflected power from the microwave bridge by varying the microwave frequency. This mode is only available in operate view while the microwave output is enabled. The lock mode will be automatically turned off upon exiting the operate view or disabling the microwave output.

The lock feature can be enabled/disabled by pressing the 3rd (bottom silver) button next to the display. When the lock feature is enabled, the text color indicating the frequency will change to orange and the word “LOCK” will be displayed.

If the microwave frequency is within a few MHz of the resonator frequency, the lock feature should automatically find the center of the dip. If the microwave frequency is not close to the resonator frequency, it is unlikely the lock feature will be able to find the dip.
The lock feature can aid in fine tuning the microwave resonator. A brief description of tuning with the lock feature is given below:

1.	Set the microwave power to 10 dBm and set the microwave frequency to within a few MHz of the resonator frequency.
2.	Make sure the operate view is displayed and enable the microwave output.
3.	Enable lock mode by pressing the 3rd button next to the display. Once enabled, the frequency should change to minimize the reflections from the resonator. This may take a moment until the frequency is centered on the dip.
4.	Once the Rx monitor has stabilized to a minimum value, slowly adjust the iris screw to minimize the reflections from the resonator (Rx).
5.	The resonator is critically coupled when the Rx monitor reading is minimized.
6.	If required, steps 4 and 5 can be repeated at higher power.

At high microwave power, e.g. > 30 dBm, the microwaves will induce significant heating in the cavity walls which will reduce the resonance frequency of the resonator. At very high powers, e.g. 40 dBm, the frequency can shift by more than 5 MHz. The lock feature is intended to compensate for temperature dependent shifts in the frequency of the resonator.
Several of the default lock parameters are accessible through the serial port. These are detailed in the remote interface section.

Microwave Amplifier :index:`Temperature Monitor`
================================================

The Bridge12 MPS continuously monitors the temperature of the microwave amplifier and disables the output power if the amplifier temperature reaches 55ºC. The amplifier temperature is displayed in the advanced view.

If the MPS frequently overheats and the microwave power is disabled, contact Bridge12 immediately at support@bridge12.com.



