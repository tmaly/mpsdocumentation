====================
:index:`Maintenance`
====================

The Bridge12 MPS does not have any internal or external parts that require frequent maintenance. If any parts of the systems do not work as intended, please contact Bridge12 immediately at support@bridge12.com

-------------------------
:index:`Firmware Upgrade`
-------------------------

.. warning::
    Firmware upgrade should only be performed after contacting Bridge12. Do not attempt to overwrite the system firmware to avoid permanent damages to the instrument.

Sometimes, it is necessary to upgrade the firmware of your system to enable new features or remove bugs of previous firmware versions. Please consult Bridge12 first to determine if a firmware upgrade is necessary.

The firmware upgrade is typically performed remotely, together with a researcher from Bridge12. To perform the firmware upgrge you will need:

* A working internet connection
* An USB-A to USB-A cable
* A computer running Win 10 or higher (we have successfully used older Windows versions e.g. Windows XP)
* Files required for firmware upgrade

A Bridge12 representative will send you several files that are required for the upgrade and will work closely with you to perform the upgrade.