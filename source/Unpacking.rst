=============================
:index:`Unpacking the System`
=============================

The Bridge12 MPS is shipped in a wooden crate to protect the system during shipping.

.. image:: _static/images/Unpacking_20204027.png
    :width: 600
    :alt: MPS Front Panel
    :align: center

After unpacking the system, Bridge12 recommends storing the wooden crate and the cardboard box in a dry place. This is the safest method of re-packing the system if the system must be returned to Bridge12 for repair or maintenance.